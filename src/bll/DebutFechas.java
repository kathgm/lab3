/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bll;

import data.Archivo;
import java.text.ParseException;
import java.util.Date;

/**
 *
 * @author Kath
 */
public class DebutFechas {
    Archivo oArchivo = new Archivo();
    String[][] resultado = new String[0][3];

    public void redimencionar(int tamaño) {
        String[][] respaldo = resultado;
        resultado = new String[respaldo.length + 1][tamaño];
        for (int i = 0; i < respaldo.length; i++) {
            for (int j = 0; j < respaldo[i].length; j++) {
                resultado[i][j] = respaldo[i][j];
            }
        }
    }
    

    public String[][] debutJugadores(String fechaInicio, String fechaFinal) throws ParseException {
        String[] contenido = oArchivo.consultaDebut(fechaInicio,fechaFinal).split("\n");
        for (int i = 0; i < contenido.length; i++) {
            String[] linea = contenido[i].split(",");
            this.redimencionar(3);
            resultado[resultado.length - 1][0] = linea[0];
            resultado[resultado.length - 1][1] = linea[1];
            resultado[resultado.length - 1][2] = linea[2];
        }
        return resultado;
    }
    
}
