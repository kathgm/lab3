/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bll;

import data.Archivo;

/**
 *
 * @author Kath
 */
public class FortalezaVelocidad {

    Archivo oArchivo = new Archivo();
    String[][] resultado = new String[0][4];

    public void redimencionar(int tamaño) {
        String[][] respaldo = resultado;
        resultado = new String[respaldo.length + 1][tamaño];
        for (int i = 0; i < respaldo.length; i++) {
            for (int j = 0; j < respaldo[i].length; j++) {
                resultado[i][j] = respaldo[i][j];
            }
        }
    }

    public String[][] jugadoresVeloces() {
        String[] contenido = oArchivo.consultaVelocidad().split("\n");
        for (int i = 0; i < contenido.length; i++) {
            String[] linea = contenido[i].split(",");
            this.redimencionar(4);
            resultado[resultado.length - 1][0] = linea[0];
            resultado[resultado.length - 1][1] = linea[1];
            resultado[resultado.length - 1][2] = linea[2];
            resultado[resultado.length - 1][3] = linea[3];
        }
        return resultado;
    }
}
