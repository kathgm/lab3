/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bll;

import data.Archivo;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kath
 */
public class Registro {

    String[] registro;
    Archivo oArchivo = new Archivo();

    public String[] equipos() {
        String[] contenido = oArchivo.consultaEquipos2().split("\n");
        String[] resultado = new String[contenido.length];
        for (int i = 0; i < contenido.length; i++) {
            String[] linea = contenido[i].split(",");
            resultado[i] = linea[0];
        }
        return resultado;
    }

    public void registroJugador(int cedula, String nombre, int edad, boolean estado,
            int equipo, int destreza, String fechaDebut) throws ParseException {
        String ced = Integer.toString(cedula);
        String anios = Integer.toString(edad);
        String estatus = Boolean.toString(estado);
        String nEquipo = Integer.toString(equipo);
        String habilidad = Integer.toString(destreza);
        registro = new String[7];
        registro[0] = ced;
        registro[1] = nombre;
        registro[2] = anios;
        registro[3] = estatus;
        registro[4] = nEquipo;
        registro[5] = habilidad;
        registro[6] = fechaDebut;
        int scedula = Integer.parseInt(registro[0]);
        String snombre = registro[1];
        int sedad = Integer.parseInt(registro[2]);
        boolean sestado = Boolean.parseBoolean(registro[3]);
        int sequipo = Integer.parseInt(registro[4]);
        int sdestreza = Integer.parseInt(registro[5]);
        String sfecha = registro[6];
        try {
            oArchivo.insertar(scedula, snombre, sedad, sestado, sequipo, sdestreza, sfecha);
        } catch (SQLException ex) {
            Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    

}
