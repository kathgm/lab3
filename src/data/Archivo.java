/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Kath
 */
public class Archivo {

    private Connection connection = null;
    private ResultSet result = null;
    private Statement statement = null;

    public Archivo() {
        Conexion();
    }

    public void Conexion() {
        if (connection != null) {
            return;
        }

        String url = "jdbc:postgresql://localhost:5432/lab_jugadores";
        String password = "Canela";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, "postgres", password);
            if (connection != null) {
                System.out.println("Connecting to database...");
            }
        } catch (Exception e) {
            System.out.println("Problem when connecting to the database");
        }
    }

    public void insertar(int cedula,String nombre,int edad,boolean estado,int equipo,int destreza,String fechaDebut) throws SQLException, ParseException {
              Date fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fechaDebut);
            statement = connection.createStatement();
            int z = statement.executeUpdate("INSERT INTO jugadores(cedula,nombre,edad,estado,equipo,destreza,fecha_debut) VALUES ('" + cedula + "', '"+nombre+"', '"+edad+"', '"+estado+"', '"+equipo+"', '"+destreza+"', '"+fecha+"')");
            if (z == 1) {
                System.out.println("Se agregó el registro de manera exitosa");
            } else {
                System.out.println("Error al insertar el registro");
            }
        
    }
    
    public String consultaVelocidad() {
        String resultado = "";
        try {
            statement = connection.createStatement();
            result = statement.executeQuery("SELECT j.cedula, j.nombre, e.nombre_equipo, d.perfil "
                    + "FROM jugadores j, equipo e, destrezas d "
                    + "WHERE  j.estado = true and j.edad >20 and d.fortaleza = 'velocidad' and j.destreza =d.id_destreza and j.equipo = id_equipo");

            while (result.next()) {
                resultado += result.getString("cedula") + ","
                        + result.getString("nombre") + ","
                        + result.getString("nombre_equipo") + ","
                        + result.getString("perfil") + "\n";//*/

            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
        return resultado;
    }

    public String consultaDebut(String fechaI, String fechaF) throws ParseException {
        Date fechaInicio = new SimpleDateFormat("yyyy-MM-dd").parse(fechaI);
        Date fechaFinal = new SimpleDateFormat("yyyy-MM-dd").parse(fechaF);

        String resultado = "";
        try {
            statement = connection.createStatement();
            result = statement.executeQuery("SELECT j.nombre, e.nombre_equipo, j.fecha_debut "
                    + "FROM jugadores j, equipo e "
                    + "WHERE j.fecha_debut >= '" + fechaInicio + "' and j.fecha_debut <= '" + fechaFinal + "' and j.equipo = e.id_equipo");

            while (result.next()) {
                resultado += result.getString("nombre") + ","
                        + result.getString("nombre_equipo") + ","
                        + result.getString("fecha_debut") + "\n";//*/

            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
        return resultado;
    }

    public String consultaRangoEdad(int edadInicio, int edadFinal, String equipo) {

        String resultado = "";
        try {
            statement = connection.createStatement();
            result = statement.executeQuery("select j.cedula, j.nombre, j.edad, j.fecha_debut "
                    + "from jugadores j, equipo e, destrezas d "
                    + "where j.edad >= '" + edadInicio + "' and j.edad <= '" + edadFinal + "' and d.fortaleza='cabeceo'"
                    + " and j.fecha_debut > '1997-01-01' and j.equipo = e.id_equipo and j.destreza = d.id_destreza "
                    + "and e.nombre_equipo='" + equipo + "'and j.destreza = d.id_destreza and j.destreza =4");
            while (result.next()) {
                resultado += result.getString("cedula") + ","
                        + result.getString("nombre") + ","
                        + result.getString("edad") + ","
                        + result.getString("fecha_debut") + "\n";//*/

            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
        return resultado;
    }

    public String consultaEquipos() {

        String resultado = "";
        try {
            statement = connection.createStatement();
            result = statement.executeQuery("SELECT nombre_equipo FROM equipo WHERE estado = true ");

            while (result.next()) {
                resultado += result.getString("nombre_equipo") + "\n";

            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
        return resultado;
    }
    
    public String consultaEquipos2() {

        String resultado = "";
        try {
            statement = connection.createStatement();
            result = statement.executeQuery("SELECT nombre_equipo FROM equipo");

            while (result.next()) {
                resultado += result.getString("nombre_equipo") + "\n";

            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
        return resultado;
    }
    /*public void modificar() {
        try {
            int cedula = Integer.parseInt(txtCedula.getText());
            int edad = Integer.parseInt(txtEdad.getText());
            int notaUno = Integer.parseInt(txtNotaUno.getText());
            int notaDos = Integer.parseInt(txtNotaDos.getText());
            int notaTres = Integer.parseInt(txtNotaTres.getText());

            statement = connection.createStatement();
            int z = statement.executeUpdate("UPDATE estudiantes SET nota_uno = '" + notaUno + "', "
                    + "nota_dos = '" + notaDos + "', nota_tres = '" + notaTres + "', "
                    + "edad_est = '" + edad + "' WHERE cedula_est = '" + cedula + "'");
            if (z == 1) {
                System.out.println("Se módificó el registro de manera exitosa");
            } else {
                System.out.println("Error al modificar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }//*/

 /*public void eliminar() {
        try {
            int cedula = Integer.parseInt(txtCedula.getText());

            statement = connection.createStatement();
            int z = statement.executeUpdate("DELETE FROM estudiantes WHERE cedula_est = '" + cedula + "'");
            if (z == 1) {
                System.out.println("Se eliminó el registro de manera exitosa");
            } else {
                System.out.println("Error al eliminar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }//*/
}
